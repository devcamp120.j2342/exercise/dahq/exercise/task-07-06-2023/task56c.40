package com.devcamp.customerinvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.model.Invoice;
import com.devcamp.customerinvoiceapi.service.InvoiceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoices")
    public ArrayList<Invoice> invoicesList() {
        ArrayList<Invoice> invoices = invoiceService.allInvoices();
        return invoices;
    }

    @GetMapping("/invoices/{invoiceId}")
    public Invoice invoiceRe(@PathVariable int invoiceId) {
        Invoice invoice = invoiceService.invoiceIndex(invoiceId);
        return invoice;
    }

}
